package no.noroff.accelerate;

import no.noroff.accelerate.vehicle.Vehicle;
import no.noroff.accelerate.vehicle.components.Engine;
import no.noroff.accelerate.vehicle.components.FuelType;
import no.noroff.accelerate.vehicle.land.Bike;
import no.noroff.accelerate.vehicle.land.Car;
import no.noroff.accelerate.vehicle.land.Driveable;

public class Main {
    public static void main(String[] args) {
        Car broomBroom = new Car(4,"Blue",8, new Engine(100, FuelType.Petrol));
        System.out.println("Hello world!");
        Vehicle car2 = new Car(4,"Blue",8, new Engine(50, FuelType.Diesel));
        Driveable car3 = new Car(4,"Blue",8, new Engine(30, FuelType.ManPower));
        Bike bike = new Bike(1,"",2);
        doDriving(bike);
        doDriving(broomBroom);
        doDriving(car3);
    }

    public static void doDriving(Driveable driveable) {
        driveable.accelerate();
    }
}