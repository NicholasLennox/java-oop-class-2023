package no.noroff.accelerate.vehicle.aquatic;

import no.noroff.accelerate.vehicle.Vehicle;

public class Boat extends Vehicle {

    public Boat(int capacity, String colour, double size) {
        super(capacity, colour, size);
    }

    @Override
    public void makeNoise() {

    }

    public void boatThings() {

    }
}
