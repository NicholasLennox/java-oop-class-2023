package no.noroff.accelerate.vehicle.air;

import no.noroff.accelerate.vehicle.Vehicle;

public class Plane extends Vehicle {
    public Plane(int capacity, String colour, double size) {
        super(capacity, colour, size);
    }

    @Override
    public void makeNoise() {

    }
}
