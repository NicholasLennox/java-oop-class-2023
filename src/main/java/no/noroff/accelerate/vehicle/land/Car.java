package no.noroff.accelerate.vehicle.land;

import no.noroff.accelerate.vehicle.Vehicle;
import no.noroff.accelerate.vehicle.air.Flyable;
import no.noroff.accelerate.vehicle.components.Engine;

public class Car extends Vehicle implements Driveable {
    private Engine engine;
    public Car(int capacity, String colour, double size, Engine engine) {
        super(capacity, colour, size);
        this.engine = engine;
    }

    @Override
    public void makeNoise() {
        System.out.println("Toot toot");
    }

    @Override
    public void getDetails() {
        super.getDetails();
        System.out.println("Engine: " + engine.isRunning());
    }

    @Override
    public void start() {
        engine.start();
    }

    @Override
    public void accelerate() {
        currentSpeed++;
        engine.useFuel();
    }

    @Override
    public void decelerate() {
        currentSpeed--;
    }

    @Override
    public void turnOff() {

    }
}
