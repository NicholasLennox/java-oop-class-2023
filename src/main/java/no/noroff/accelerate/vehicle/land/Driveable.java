package no.noroff.accelerate.vehicle.land;

public interface Driveable {
    void start();
    void accelerate();
    void decelerate();
    void turnOff();
}
