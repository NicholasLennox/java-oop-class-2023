package no.noroff.accelerate.vehicle.land;

import no.noroff.accelerate.vehicle.Vehicle;

public class Bike extends Vehicle implements Driveable{
    public Bike(int capacity, String colour, double size) {
        super(capacity, colour, size);
    }

    @Override
    public void makeNoise() {

    }

    @Override
    public void start() {

    }

    @Override
    public void accelerate() {

    }

    @Override
    public void decelerate() {

    }

    @Override
    public void turnOff() {

    }
}
