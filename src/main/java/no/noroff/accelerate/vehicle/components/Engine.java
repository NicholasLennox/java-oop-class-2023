package no.noroff.accelerate.vehicle.components;

public class Engine {
    private int capacity;
    private FuelType fuelType;

    private boolean isRunning;

    public Engine(int capacity, FuelType fuelType) {
        this.capacity = capacity;
        this.fuelType = fuelType;
    }

    public void start() {
        System.out.println("starting");
    }

    public void useFuel() {
        System.out.println("omnomnomnom");
    }

    public boolean isRunning() {
        return isRunning;
    }
}
