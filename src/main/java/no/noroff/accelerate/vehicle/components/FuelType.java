package no.noroff.accelerate.vehicle.components;

public enum FuelType {
    Petrol,
    Electricity,
    Hydrogen,
    Diesel,
    ManPower
}
