package no.noroff.accelerate.vehicle;

public abstract class Vehicle {
    protected int capacity;
    protected String colour;
    protected double size;
    protected double currentSpeed;
    protected double orientation;

    public Vehicle(int capacity, String colour, double size) {
        this.capacity = capacity;
        this.colour = colour;
        this.size = size;
        currentSpeed = 0;
        orientation = 0;
    }

    public abstract void makeNoise();

    public void getDetails() {
        System.out.println("Capacity: " + capacity);
    }

}
